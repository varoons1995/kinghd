const express = require('express');
const router = express.Router();
const home = require('./home');
const error = require('./error');
const movie = require('./movie');
const category = require('./category');
const search = require('./search');
const addmovie = require('./addmovie');


// GET home page.
router.route('/')
	.get(home.get);

router.route('/movie/:title')
	.get(movie.get);

router.route('/category/:category')
	.get(category.get);

router.route('/search')
	.get(search.get);

router.route('/search/:search')
	.get(search.get);

router.route('/superadmin/naja/jubjub')
	.get(addmovie.get).post(addmovie.post)


// stay last in case of page error
router.route('*')
	.get(error.get404);


module.exports = router;
