const categoryRoutes = {};
const dbmovie = require('../../lib/dbmovie');
const util = require('../../lib/utility');
const mPagination = require('../../lib/mPagination');

categoryRoutes.get = (req, res) => {
    let currentPage = util.toNum( req.query.page, 1 );
    let offset = (currentPage-1)*21;
    
    Promise.all([dbmovie.getByCategory(offset, req.params.category), dbmovie.getCategory(), dbmovie.countByCategory(req.params.category),dbmovie.getMovie12item(),dbmovie.getMovie12item(),dbmovie.getMovie12item()])
    .then((values) => {
        let basedUrl = req._parsedOriginalUrl.pathname;
        let list_movie = values[0];
        let category = values[1];
        let count = values[2][0].count;
        let movie1 = values[3];
        let movie2 = values[4];
        let movie3 = values[5];
        let maxPage = Math.ceil(count/21);
        let paginateOption = {
            a_class: ['page-link'],
            li_class: ['page-item'],
            ul_class: ['pagination', 'justify-content-center']
        };
        let nav_pagination = mPagination.getPagination(currentPage,maxPage,basedUrl,paginateOption);

        res.render('listMovie', {
            req: req,
            list_movie: list_movie,
            category: category,
            nav_pagination: nav_pagination,
            movie1:movie1,
            movie2:movie2,
            movie3:movie3,
        });

    })
    .catch((error) => {
        console.error(error.message);
        res.render('error');
    });
}

module.exports = categoryRoutes;
