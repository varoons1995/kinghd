const movieRoutes = {};
const dbmovie = require('../../lib/dbmovie');
const util = require('../../lib/utility');

movieRoutes.get = (req, res) => {
    dbmovie.getByTitle(req.params.title)
    .then((value) => {
        let movie = value[0];

        Promise.all([dbmovie.getByTitle(req.params.title), dbmovie.getCategory(), dbmovie.getVideoByMovieId(movie.id), dbmovie.getcategoryByMovieId(movie.id)])
        .then((values) => {
            let category = values[1];
            let video = values[2];
            let movieCategory = values[3];

            res.render('movie', {
                req: req,
                movie: movie,
                category: category,
                video: video,
                movieCategory: movieCategory
            });
        })

    })
    .catch(error => {
        console.error(error.message);
        res.render('error');
    });

    // Promise.all([dbmovie.getByTitle(req.params.title), dbmovie.getCategory()])
    // .then((values) => {
    //     let movie = values[0][0];
    //     let category = values[1];

    //     res.render('movie', {
    //         req: req,
    //         movie: movie,
    //         category: category
    //     });
    // })
    // .catch(error => {
    //     console.error(error.message);
    //     res.render('error');
    // });
}

module.exports = movieRoutes;
