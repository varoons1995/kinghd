function toNum(num, def=0) {
    let mNum = Number(num);
    if( !isNaN(mNum) && mNum > 0 && Number.isInteger(mNum)){
        return mNum;
    }else {
        return def;
    }
}

function splitQuery(query) {
    let result = [];
    let arrQuery = query.str.split("&");
    for(const arr of arrQuery){
        let arrTemp = arr.split('=');
        result.push({[arrTemp[0]]: arrTemp[1]});
    }
    return result;
}

module.exports = {
    toNum
}